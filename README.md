# Ubatubot

Bot de telegram para @s maluc@s do Tropixel.

## Como faz

Este bot precisa de um token para conectar no Telegram. Converse com o [BotFather](https://t.me/botfather)
para registrar seu bot e pegar seu token. Coloque esse token em uma variável de ambiente -- pra desenvolvimento,
use o arquivo `.env`, coloque algo tipo `TELEGRAM_API_KEY=nanana-nanana` e pode rodar seu bot.

[Poetry](https://python-poetry.org/) é a bola da vez no mundo Python. Instale
como quiser e depois rode `poetry install` para instalar tudo que este bot
precisa em um virtualenv certinho. 

Então pra rodar, basta executar `poetry run ubatubot`.
