#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from dotenv import load_dotenv
from telegram.ext import Updater
from . import boteco


def main():
    '''Configura e roda o bot.'''
    updater = Updater(token=os.environ.get('TELEGRAM_API_KEY'),
                      use_context=True)
    updater.dispatcher.add_handler(boteco.handler)
    updater.start_polling()


if __name__ == '__main__':
    load_dotenv()
    main()
