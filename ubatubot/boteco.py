# -*- coding: utf-8 -*-

import os
from telegram.ext import CommandHandler


def boteco(update, context):
    '''Informa o link e a senha para o boteco.'''
    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=f"Link pro boteco: {os.environ.get('BOTECO_URL')}.")


handler = CommandHandler('boteco', boteco)
